# Generated by Django 3.1.7 on 2023-06-02 10:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DeCharla', '0019_auto_20230518_1652'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='font_family',
            field=models.CharField(default='Arial', max_length=50),
        ),
        migrations.AddField(
            model_name='session',
            name='font_size',
            field=models.CharField(default='12px', max_length=20),
        ),
    ]
