var container = document.getElementById('messages-container');
var messages = document.getElementById('messages');

// Función para desplazar el contenido hacia abajo
function scrollDown() {
  container.scrollTop = container.scrollHeight;
}

// Desplazar el contenido hacia abajo al cargar la página
window.addEventListener('load', scrollDown);

// Desplazar el contenido hacia abajo cada vez que se agregue un nuevo mensaje
messages.addEventListener('DOMNodeInserted', scrollDown);


