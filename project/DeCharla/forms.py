
from django import forms

class RoomForm(forms.Form):
    room_name = forms.CharField(max_length=100)

class LoginForm(forms.Form):
    password = forms.CharField(max_length=100)

