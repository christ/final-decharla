from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('favicon.ico', views.get_favicon, name='favicon'),
    path('', views.login_view, name='login'),
    path('room_list/', views.room_list, name='room_list'),
    path('rooms/<int:room_id>/', views.room, name='room'),
    path('rooms/<int:room_id>/send_message/', views.send_message, name='send_message'),
    path('rooms/<int:room_id>/messages.json', views.messages_json, name='messages_json'),
    path('rooms/<int:room_id>/roomdin/', views.roomdin, name='roomdin'),
    path('rooms/<int:room_id>/send_message_din/', views.send_message_din, name='send_message_din'),
    path('configuracion/', views.configuracion, name='configuracion'),
    path('ayuda/', views.ayuda, name='ayuda'),
    path('logout/', views.logout_view, name='logout_view'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
