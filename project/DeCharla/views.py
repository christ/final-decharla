import random
import string
from xml.etree import ElementTree
from django.shortcuts import redirect, render, get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.core import validators
from django.core.exceptions import ValidationError
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from .models import Room, Message, Passwords, Session, VisitedChat
import xml.etree.ElementTree as ET
import os
from django.conf import settings
from django.db.models import Count


# Función para verificar la sesión del usuario
def check_session(request):
    cookie = request.COOKIES.get("session")
    if cookie is None:
        # Si no existe la cookie, se crea una nueva y se devuelve la respuesta con la cookie establecida
        response = HttpResponse()
        random_cookie = ''.join(random.choices(string.ascii_lowercase + string.digits, k=128))
        response.set_cookie("session", random_cookie)
        return response
    if request.path != "/login":
        # Verificar si la sesión existe en la base de datos
        check = Session.objects.filter(session=cookie).exists()
    else:
        # Si la página es la de inicio de sesión, se considera que la sesión es válida
        check = True
    return check


# Vista de inicio
def index(request):
    # Verificar la sesión del usuario
    check = check_session(request)
    if not check:
        # Si no hay sesión válida, redirigir a la página de inicio de sesión
        return redirect('login')

    # Redirigir a la lista de salas
    return redirect('room_list')

# Vista de inicio de sesión
def login_view(request):
    if request.user.is_authenticated:
        # Si el usuario ya está autenticado, redirigir a la lista de salas
        return redirect('room_list')

    if request.method == 'POST':
        password = request.POST.get('password')

        if Passwords.objects.filter(password=password).exists():
            try:
                session = Session.objects.create(
                    session=''.join(random.choices(string.ascii_lowercase + string.digits, k=128)), name="Anónimo")
            except Session.DoesNotExist:
                session = Session.objects.create(session=''.join(random.choices(string.ascii_lowercase + string.digits, k=128)), name="Anónimo")

            # Establecer la sesión y el nombre de usuario en la sesión actual del usuario
            request.session['logged_in'] = True
            request.session['username'] = session.name

            response = redirect('room_list')
            response.set_cookie('session', session.session)  # Crear la cookie con el valor de la sesión
            return response
        else:
            messages.error(request, 'Contraseña incorrecta')

    return render(request, 'login.html')

# Vista de cierre de sesión
def logout_view(request):
    # Limpiar la sesión y borrar la cookie de sesión
    request.session.clear()
    response = redirect('login')
    response.delete_cookie('session')  # Borrar la cookie 'sessionid'
    return response

# Vista de la lista de salas
def room_list(request):
    # Verificar la sesión del usuario
    check = check_session(request)
    if not check:
        # Si no hay sesión válida, redirigir a la página de inicio de sesión
        return redirect('login')

    if request.method == 'GET':
        room_name = request.GET.get('room_name')
        if room_name:
            # Crear una nueva sala y redirigir a la sala recién creada
            new_room = Room.objects.create(name=room_name)
            return redirect('room', room_id=new_room.id)

    # Obtener las salas con mensajes y contar el número de mensajes e imágenes
    rooms = Room.objects.annotate(num_messages=Count('message')).filter(num_messages__gt=0)
    messages_count = Message.objects.filter(room__in=rooms, is_image=False).count()
    images_count = Message.objects.filter(room__in=rooms, is_image=True).count()

    # Obtener el nombre de usuario de la sesión o establecerlo como "Anónimo"
    username = request.session.get('username', 'Anónimo')

    context = {
        'rooms': rooms,
        'rooms_count': rooms.count(),
        'messages_count': messages_count,
        'images_count': images_count,
        'username': username,
    }
    return render(request, 'room_list.html', context)

# Vista de una sala
@csrf_exempt
def room(request, room_id):
    # Verificar la sesión del usuario
    check = check_session(request)
    if not check:
        # Si no hay sesión válida, redirigir a la página de inicio de sesión
        return redirect('login')

    # Obtener la sala correspondiente al ID proporcionado o mostrar una página de error 404 si no existe
    room = get_object_or_404(Room, id=room_id)
    messages = Message.objects.filter(room=room).order_by('-timestamp')[:50]

    image_url = None
    for message in messages:
        if message.is_image:
            image_url = message.image_url
            break

    if image_url:
        try:
            validators.URLValidator()(image_url)
        except ValidationError:
            image_url = None

    context = {
        'room': room,
        'messages': messages,
        'image_url': image_url,
    }
    return render(request, 'room.html', context)

# Vista para enviar un mensaje a una sala
@csrf_exempt
def send_message(request, room_id):
    # Verificar la sesión del usuario
    check = check_session(request)
    if not check:
        # Si no hay sesión válida, redirigir a la página de inicio de sesión
        return redirect('login')

    room = get_object_or_404(Room, id=room_id)
    sender = request.session.get('username', 'Anónimo')

    if request.method == 'PUT':
        # Procesar los datos del mensaje en formato XML



        xml_string = request.body.decode('utf-8')
        messages_data = xml_parser(xml_string)

        for message_data in messages_data:
            text = message_data['text']
            is_image = message_data['isimg']
            image_url = None

            if is_image:
                try:
                    validators.URLValidator()(text)
                    image_url = text
                    # Reemplazar el texto del mensaje con la etiqueta <img>
                    text = ''
                except ValidationError:
                    is_image = False

            # Crear y guardar la instancia de Message en la base de datos
            message = Message.objects.create(text=text, is_image=is_image)
            message.save()

        return redirect('room', room_id=room_id)

    elif request.method == 'POST':
        text = request.POST.get('message')
        is_image = bool(request.POST.get('is_image', False))
        image_url = request.POST.get('image_url', '')

        if is_image and text:
            try:
                validators.URLValidator()(text)
                image_url = text
                # Reemplazar el texto del mensaje con la etiqueta <img>
                text = ''
            except ValidationError:
                is_image = False

        message = Message(text=text, is_image=is_image, room=room, sender=sender)
        message.image_url = image_url
        message.save()

        return redirect('room', room_id=room_id)


# Función para procesar mensajes en formato XML
def xml_parser(xml_string):
    root = ET.fromstring(xml_string)
    messages = []
    for msg in root.findall('message'):
        isimg = msg.get('isimg')
        if isimg == "true":
            is_img = True
        else:
            is_img = False
        text = msg.find('text').text.strip()  # Eliminar espacios en blanco alrededor del texto
        messages.append({'isimg': is_img, 'text': text})
    return messages




# Vista de configuración del usuario
def configuracion(request):
    # Verificar la sesión del usuario
    check = check_session(request)
    if not check:
        # Si no hay sesión válida, redirigir a la página de inicio de sesión
        return redirect('login')

    if request.method == 'POST':
        new_username = request.POST.get('username')
        request.session['username'] = new_username

        font_size = request.POST.get('font_size')
        font_type = request.POST.get('font_type')

        # Guardar el tamaño y tipo de letra en la sesión del usuario
        request.session['font_size'] = font_size
        request.session['font_type'] = font_type

        return redirect('room_list')
    else:
        # Obtener los valores de tamaño y tipo de letra de la sesión
        font_size = request.session.get('font_size', '12px')
        font_type = request.session.get('font_type', 'Arial')

        # Obtener el nombre de usuario de la sesión o establecerlo como "Anónimo"
        username = request.session.get('username') or 'Anónimo'

        context = {
            'font_size': font_size,
            'font_type': font_type,
            'username': username,
        }
        return render(request, 'config.html', context)

# Vista de ayuda
def ayuda(request):
    # Verificar la sesión del usuario
    check = check_session(request)
    if not check:
        # Si no hay sesión válida, redirigir a la página de inicio de sesión
        return redirect('login')

    return render(request, 'help.html')

# Vista para obtener el favicon del sitio web
def get_favicon(request):
    favicon_path = os.path.join(settings.STATIC_ROOT, 'image/favicon.png')
    with open(favicon_path, 'rb') as favicon:
        response = HttpResponse(favicon.read(), content_type='image/png')
    return response

# Vista para obtener los mensajes de una sala en formato JSON
def messages_json(request, room_id):
    room = get_object_or_404(Room, id=room_id)
    messages = Message.objects.filter(room=room).order_by('timestamp')

    messages_json = []

    for message in messages:
        message_data = {
            'author': message.sender,
            'text': message.text,
            'isimg': message.is_image,
            'date': message.timestamp.strftime('%Y-%m-%d:%H:%M:%S')
        }
        messages_json.append(message_data)

    return JsonResponse(messages_json, safe=False)

# Vista de una sala dinámica
def roomdin(request, room_id):
    # Verificar la sesión del usuario
    check = check_session(request)
    if not check:
        # Si no hay sesión válida, redirigir a la página de inicio de sesión
        return redirect('login')

    # Obtener la sala correspondiente al ID proporcionado o mostrar una página de error 404 si no existe
    room = get_object_or_404(Room, id=room_id)
    messages = Message.objects.filter(room=room).order_by('-timestamp')[:50]

    image_url = None
    for message in messages:
        if message.is_image:
            image_url = message.image_url
            break

    if image_url:
        try:
            validators.URLValidator()(image_url)
        except ValidationError:
            image_url = None

    context = {
        'room': room,
        'messages': messages,
        'image_url': image_url,
    }
    return render(request, 'roomdin.html', context)

def send_message_din(request, room_id):
    # Verificar la sesión del usuario
    check = check_session(request)
    if not check:
        # Si no hay sesión válida, redirigir a la página de inicio de sesión
        return redirect('login')

    room = get_object_or_404(Room, id=room_id)
    sender = request.session.get('username', 'Anónimo')

    if request.method == 'PUT':
        # Procesar los datos del mensaje en formato XML
        xml_string = request.body.decode('utf-8')
        messages_data = xml_parser(xml_string, room_id)

        for message_data in messages_data:
            text = message_data['text']
            is_image = message_data['isimg']
            image_url = None

            if is_image:
                try:
                    validators.URLValidator()(text)
                    image_url = text
                    # Reemplazar el texto del mensaje con la etiqueta <img>
                    text = ''
                except ValidationError:
                    is_image = False

            # Crear y guardar la instancia de Message en la base de datos
            Message.objects.create(room=room, sender=sender, text=text, is_image=is_image, image_url=image_url)

        return redirect('roomdin', room_id=room_id)

    elif request.method == 'POST':
        text = request.POST.get('message')
        is_image = bool(request.POST.get('is_image', False))
        image_url = request.POST.get('image_url', '')

        if is_image and text:
            try:
                validators.URLValidator()(text)
                image_url = text
                # Reemplazar el texto del mensaje con la etiqueta <img>
                text = ''
            except ValidationError:
                is_image = False

        message = Message(text=text, is_image=is_image, room=room, sender=sender)
        message.image_url = image_url
        message.save()

        return redirect('roomdin', room_id=room_id)


def get_last_read_messages(session, room):
    visited_chat = VisitedChat.objects.filter(session=session, chat=room).order_by('-access_date').first()

    if visited_chat:
        last_read_date = visited_chat.access_date
        last_read_messages = Message.objects.filter(room=room, timestamp__gt=last_read_date).order_by('timestamp')
    else:
        # El usuario no ha leído ningún mensaje en esta sala
        last_read_messages = Message.objects.none()

    return last_read_messages

