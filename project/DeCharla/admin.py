from django.contrib import admin
from .models import Room, Message, Session, Passwords

admin.site.register(Room)
admin.site.register(Message)
admin.site.register(Session)
admin.site.register(Passwords)