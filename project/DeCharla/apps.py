from django.apps import AppConfig


class DecharlaConfig(AppConfig):
    name = 'DeCharla'
    default_auto_field = "django.db.models.BigAutoField"