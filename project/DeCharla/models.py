from django.db import models

class Room(models.Model):
    name = models.TextField()
    #creator = models.TextField(default='')
    #description = models.TextField(default='')

class Message(models.Model):
    session = models.TextField(default="")
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    sender = models.TextField(default='')
    text = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    is_image = models.BooleanField(default=False)
    image_url = models.URLField(null=True, blank=True)

class Passwords(models.Model):
    password = models.CharField(max_length=128)

class Session(models.Model):
    session = models.TextField(unique=True)
    name = models.TextField()
    font_size = models.CharField(max_length=20, default='12px')
    font_family = models.CharField(max_length=50, default='Arial')



class VisitedChat(models.Model):
    chat = models.ForeignKey(Room, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    access_date = models.DateTimeField(auto_now=True)