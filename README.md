# Final DeCharla

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Christian Marina Villasevil
* Titulación: Ingenieria Telemática
* Cuenta en laboratorios: christ
* Cuenta URJC: c.marina.2019
* Video básico (url): https://youtu.be/GQSnz2AlghU
* Video parte opcional (url): https://youtu.be/238KAHpzsCk
* Despliegue (url): https://christ07.pythonanywhere.com/DeCharla/
* Contraseñas: st, python, urjc
* Cuenta Admin Site: admin/admin


## Resumen parte obligatoria

En la parte obligatoria podemos observar que al introducirnos en la url de la práctica, tendremos un formulario de login, el cual introduciremos nuestras contraseñas para que nos redirija al creador de salas y nos mostraría las salas disponibles que tenemos en ese momento.
En el creador de salas podríamos poner cualquier nombre de sala, y como la sala nada más crearla no consta de ningún mensaje, nos redirijirá a esa sala, dentro de ella podremos enviar mensajes e imágenes ( las imágenes con su adecuada url), pero el nombre de usuario que nos enviaría los mensajes sería anónimo, para poder cambiar el nombre de usuario iríamos a la pagina de Configuración y ahí tendremos un formulario, el cual rellenaríamos poniendo el nombre con el cual queremos mandar los mensajes. También tendremos la pagina de Ayuda, la cual nos muestra un pequeño funcionamiento de la página web, el autor de ella y el objetivo de la práctica.

También tenemos para todas las salas un apartado JSON que nos redirijirá a los mensajes que estan enviados en esa sala, y nos mostrará. el autor, el mensaje en cuestión, si es una imagen o no y la fecha del mensaje.

 Otra parte añadida a la práctica es la sala dinamica cada una de las salas tiene un botón dentro de ellas, en la cual podemos introducirnos en su sala dinamica, la cual su funcionamiento nos recargaria la sala cada 30 segundos para actualizar los mensajes enviados o recibidos.

Por otro lado, tenemos también la parte de la Configuración nombrada anteriormente, en la cual podemos cambiar nuestro nombre de usuario, que por defecto es anónimo y también podremos cambiar tanto el tamaño de letra como el tipo de letra en la página web.

## Lista partes opcionales

* Nombre parte: Favicon, sería el logo que podemos observar arriba del buscador de la pagina web.
* Nombre parte: Logout, el funcionamiento de logout, tendremos en la barra principal, un boton de logout, el cual cuando lo pulsemos nos volverá a mandar a la página principal con el formulario de login.

